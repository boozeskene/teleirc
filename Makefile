.PHONY: build up start stop restart

build:
	docker build -t t101/teleirc extras
	docker run --name=teleirc-data -v /home/t101/t2i/.teleirc:/home/teleirc/.teleirc t101/teleirc echo 'data teleirc'

up:
	docker run --name=teleirc -p 9090:9090 -d --volumes-from=teleirc-data t101/teleirc

start:
	docker start teleirc

stop:
	docker stop teleirc

restart:
	docker stop teleirc
	docker start teleirc
